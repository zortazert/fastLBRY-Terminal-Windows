#####################################################################
#                                                                   #
#  THIS IS A SOURCE CODE FILE FROM A PROGRAM TO INTERACT WITH THE   #
# LBRY PROTOCOL ( lbry.com ). IT WILL USE THE LBRY SDK ( lbrynet )  #
# FROM THEIR REPOSITORY ( https://github.com/lbryio/lbry-sdk )      #
# WHICH I GONNA PRESENT TO YOU AS A BINARY. SINCE I DID NOT DEVELOP #
# IT AND I'M LAZY TO INTEGRATE IN A MORE SMART WAY. THE SOURCE CODE #
# OF THE SDK IS AVAILABLE IN THE REPOSITORY MENTIONED ABOVE.        #
#                                                                   #
#      ALL THE CODE IN THIS REPOSITORY INCLUDING THIS FILE IS       #
# (C) J.Y.Amihud and Other Contributors 2021. EXCEPT THE LBRY SDK.  #
# YOU CAN USE THIS FILE AND ANY OTHER FILE IN THIS REPOSITORY UNDER #
# THE TERMS OF GNU GENERAL PUBLIC LICENSE VERSION 3 OR ANY LATER    #
# VERSION. TO FIND THE FULL TEXT OF THE LICENSE GO TO THE GNU.ORG   #
# WEBSITE AT ( https://www.gnu.org/licenses/gpl-3.0.html ).         #
#                                                                   #
# THE LBRY SDK IS UNFORTUNATELY UNDER THE MIT LICENSE. IF YOU ARE   #
# NOT INTENDING TO USE MY CODE AND JUST THE SDK. YOU CAN FIND IT ON #
# THEIR OFFICIAL REPOSITORY ABOVE. THEIR LICENSE CHOICE DOES NOT    #
# SPREAD ONTO THIS PROJECT. DON'T GET A FALSE ASSUMPTION THAT SINCE #
# THEY USE A PUSH-OVER LICENSE, I GONNA DO THE SAME. I'M NOT.       #
#                                                                   #
# THE LICENSE CHOSEN FOR THIS PROJECT WILL PROTECT THE 4 ESSENTIAL  #
# FREEDOMS OF THE USER FURTHER, BY NOT ALLOWING ANY WHO TO CHANGE   #
# THE LICENSE AT WILL. SO NO PROPRIETARY SOFTWARE DEVELOPER COULD   #
# TAKE THIS CODE AND MAKE THEIR USER-SUBJUGATING SOFTWARE FROM IT.  #
#                                                                   #
#####################################################################

# Preparing the executable of lbrynet to be running. If we don't that
# there will be a nasty error of permission being denied.

import os
os.system("chmod u+x flbry/lbrynet")

from flbry.variables import *

# A welcome logo. 
logo()

# Here I want to make a simple check for an operating system
# The software is built to work on GNU / Linux so I need to
# check whether the user runs it on a proper system. If not
# give them a warning message.

import platform
if platform.system() != "Linux": # IK It should be GNU / Linux
    center("OS "+platform.system().upper()+" NOT SUPPORTED!", "bdrd", True)
    center("Type 'osinfo' to learn more.", "bdrd")


# Importing all kinds of other things needed for the operations
from flbry import connect
from flbry import search
from flbry import channel
from flbry import wallet
from flbry import uploads
from flbry import list_files
from flbry import markdown
from flbry import trending
from flbry import url
from flbry import publish



# Now we gonna start the main loop. It will give the user to input
# any function. And when the function is executed, it will give it
# again. Forever. Until the user exits.

while True:
    command = input(typing_dots()) # the : will be the presented function

    if command == "exit":
        connect.stop()
        break # breaks the while True: loop
    
    elif command == "quit":
        print("    Quit does not disconnect the SDK!")
        print("    To disconnet use exit or disconnect.")
        break
    
    elif command == "help":
        markdown.draw("help/main.md", "Help")

    elif command == "osinfo":
        markdown.draw("help/os.md", "Operating System Information")


    # HELP AND CONTRIBUTION FUNCTIONS
    
    elif command == "matrix":
        print("    #FastLBRY:matrix.org")

    elif command == "clear":
        os.system("clear")

    elif command == "repository":
        print("    https://notabug.org/jyamihud/FastLBRY-terminal")

    elif command == "report":
        print("    https://notabug.org/jyamihud/FastLBRY-terminal/issues")

    elif command == "license":
       markdown.draw("LICENSE.md", "License (GPLv3 or later)")

    # LBRY COMMANDS

    elif command == "connect":
        connect.start()
    
    elif command == "disconnect":
        connect.stop()

    elif command.startswith("publish"):
        if " " in command:
            publish.configure(command[command.find(" ")+1:])
        else:
            publish.configure()
    
    elif command == "history":
        list_files.downloaded()

    elif command.startswith("search"):
        if " " in command:
            search.simple(command[command.find(" ")+1:])
        else:
            search.simple()

    elif command == "channels":
        channel.simple(channel.select())
            
    elif command.startswith("channel"):
        if " " in command:
            channel.simple(command[command.find(" ")+1:])
        else:
            channel.simple()

    elif command.startswith("trending"):
        trending.simple()

    elif command.startswith("articles"):
        trending.simple(articles=True)
            
    ###### WALLET ######

    elif command == "login":
        markdown.draw("help/login.md", "Login Help")
    
    elif command == "wallet":
        wallet.history()

    elif command == "uploads":
        uploads.simple()



    # If a user types anything ELSE, except just simply pressing
    # Enter. So if any text is in the command, but non of the
    # above were activated.

    # Here I want to just run the URL module and try to resolve
    # the url. The Url module will need to be able to handle a
    # lot of it.
    
    elif command:
        url.get(command)
